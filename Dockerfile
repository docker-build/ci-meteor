FROM node:14

LABEL maintainer="Jerry <jerry@wjz.im>"

# build arguments
ARG METEOR_REL=${METEOR_REL}
ARG APP_USER=node

# run environment
ENV APP_PORT=${APP_PORT:-3000}
ENV APP_ROOT=${APP_ROOT:-/node/application}

# exposed ports and volumes
EXPOSE $APP_PORT
VOLUME $APP_ROOT

# add builds for gitlab ci
RUN mkdir /builds && chown -Rh ${APP_USER} /builds


# install Meteor
RUN curl https://install.meteor.com/?release=${METEOR_REL} | sh

# run Meteor from the app directory
WORKDIR ${APP_ROOT}

# install build tools
# make python3 to default
RUN apt-get update || : && apt-get install c++ build-essential -y &\
    ln -sf /usr/bin/python3 /usr/bin/python & \
    ln -sf /usr/bin/pip3 /usr/bin/pip